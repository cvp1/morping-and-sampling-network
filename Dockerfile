#FROM registry.gitlab.com/cvp1/morping-and-sampling-network/cuda_base:latest
FROM cuda_base:latest

# Set environment to "train" or "test", depending on which stage you want to execute
ENV ENVIRONMENT="train"

SHELL ["/bin/bash", "-c"]
# Copy requirements.txt before copying the rest of the sources to hep pip caching dependencies
COPY ./requirements.txt /app/

# Change working directory. All subsequent commands will be run inside /app
WORKDIR /app
# Create venv
RUN python3 -m venv ./.venv

# Activate venv
RUN . /app/.venv/bin/activate \
    && /app/.venv/bin/pip install -r requirements.txt

# Copy sources that need to be compiled on cuda
COPY ./src/compile_cuda_sources.sh ./src/
COPY ./src/emd ./src/emd
COPY ./src/expansion_penalty ./src/expansion_penalty
COPY ./src/MDS ./src/MDS
# Make shell script executable and execute it (this compiles the CUDA sources as described in the repo's readme)
RUN . /app/.venv/bin/activate && chmod +x /app/src/compile_cuda_sources.sh && sh /app/src/compile_cuda_sources.sh

COPY ./src/*.py ./src/*.json  ./src/
COPY ./src/ycb_util ./src/ycb_util
COPY ./src/fps_util ./src/fps_util
COPY ./scripts/* ./
RUN chmod +x ./run.sh

CMD /bin/bash -c ./run.sh