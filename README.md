# MSN: Morphing and Sampling Network for Dense Point Cloud Completion
*Adapted from [the original repo](https://github.com/Colin97/MSN-Point-Cloud-Completion)*

## Installation instructions
### Install via pip
```bash
pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html
```
### Install in new conda environment
```bash
conda env create --prefix ./msn-env -f environment.yml
```

Also follow compilation instructions below.

[[paper]](http://cseweb.ucsd.edu/~mil070/projects/AAAI2020/paper.pdf) [[data]](https://drive.google.com/drive/folders/1X143kUwtRtoPFxNRvUk9LuPlsf1lLKI7?usp=sharing)

MSN is a learning-based shape completion method which can preserve the known structures and generate dense and evenly distributed point clouds. See our AAAI 2020 [paper](http://cseweb.ucsd.edu/~mil070/projects/AAAI2020/paper.pdf) for more details.

In this project, we also provide an implementation for the Earth Mover's Distance (EMD) of point clouds, which is based on the auction algorithm and only needs $O(n)$ memory.

![](/teaser.png)
*with 32,768 points after completion*

### Usage (Docker)
#### Prerequisites
- Install docker ([guide for Ubuntu 18.04](https://phoenixnap.com/kb/how-to-install-docker-on-ubuntu-18-04)). 
If you get a permission denied error when trying to run a docker image, see [here](https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue).
- Install the [nvidia-container-runtime](https://github.com/NVIDIA/nvidia-docker) to access NVIDIA GPUs.
- Verify you can access the host's GPU by running `docker run --gpus all nvidia/cuda:10.0-base nvidia-smi`.

#### Local docker build & usage 
> The docker builds using the Community Edition Gilab pipeline are very slow recently. 
Hence the recommendation is to first build locally until you create a running image, and then 
run the pipeline in the repo again.
- cd into the project root dir and then into docker_base_cuda/. Execute `docker build -t cuda_base:latest .`. 
This will build the base image containing the correct environment for the repo (CUDA 10.0, Python 3.7, GCC compilers etc.)
- cd back into the root dir. Open the Dockerfile and comment out line 1 while uncommenting line 2. This will base the 
repo's docker image on the local base image you just built. **DO NOT COMMIT & PUSH THIS CHANGE TO THE FILE!**
- Execute `docker build -t morphing_and_sampling_network:latest .` to build the docker image. 
- Locate your local path to the YCB point clouds (parent_dir of pc_complete and pc_partial). Inside this directory, 
a new directory called `morphing_and_sampling_output` will be created by the training script as a log output path for the network.

##### Completion
- go to the docker_base_cuda subfolder and run `bash build-docker-base-image.sh`
- in the root directory run `bash build-docker-image.sh`
- open `run-docker-container.sh`:
    - edit `DEFAULT_HOST_MOUNT_DIR` if needed in order to customize the local directory you want to be watched
    - edit `MODEL_PATH` if applicable to where it lies relative to the mounted local directory
- create a subfolder name `pc_complete` inside the mounted directory and add the ground truth point cloud .pcd-files
- create a `<frame-number>-config.json` with following content:
    - `{"frame_id": "000379", "file_names": ["000379-color.png", "000379-depth.png", "000379-meta.mat"], "class_id": "10", "class_name": "011_banana"}`
    - adapt the class_name in the end of the dict to the object that you wish to complete
    - the frame-number in the file name must match the one of your query point cloud
- drag and drop (copy-paste will not work!) this config as well as your query point cloud into the watched directory
    - make sure the point cloud file is called `<frame-number>-pred_cloud.pcd`

##### Train
- Run the container with the following command, where <local_YCB_path> needs to be replaced with the YCB point cloud dir:
```bash
    docker run -v <local_YCB_path>:/app/ycb_data -p 127.0.0.1:8097:8097/tcp --gpus all -it morphing_and_sampling_network:latest
```
This will start the training script via `run_training.sh` automatically.
- Open `localhost:8097` in your browser - you should see the visdom environment and plots from the network training.
- If something doesn't work with the paths / training options, check out `run_training.sh` (e.g. for higher batchSize).
> Mind that if you change scripts you need to rebuild the docker image in order for them to be present in their current 
> state inside the container.

##### Test
- Run the container with the following command, where <local_YCB_path> needs to be replaced with the YCB point cloud dir:
```bash
    docker run -v <local_YCB_path>:/app/ycb_data -p 127.0.0.1:8097:8097/tcp --gpus all -it morphing_and_sampling_network:latest /bin/bash
```
This will connect you to the docker container via bash, putting the context into the `/app` directory.
- Run `bash run_test.sh` to run the test script with the parameters specified.
- Open `localhost:8097` in your browser - you should see the visdom environment and plots from the network testing.
> Mind that if you change scripts you need to rebuild the docker image in order for them to be present in their current 
> state inside the container. Also, if you want to save output to your local machine, please use a directory 
> under /app/ycb_data. This is a local volume from your PC mounted into the docker container. All other data saved 
> only in the container will be deleted after it is stopped!
### Usage (original)

#### 1) Envrionment & prerequisites

- Pytorch 1.2.0
- CUDA 10.0
- Python 3.7
- [Visdom](https://github.com/facebookresearch/visdom)
- [Open3D](http://www.open3d.org/docs/release/index.html#python-api-index)

#### 2) Compile

Compile our extension modules:  

    cd emd
    python3 setup.py install
    cd expansion_penalty
    python3 setup.py install
    cd MDS
    python3 setup.py install

#### 3) Download data and trained models

Download the data and trained models from [here](https://drive.google.com/drive/folders/1X143kUwtRtoPFxNRvUk9LuPlsf1lLKI7?usp=sharing).  We don't provide the partial point clouds of the training set due to the large size. If you want to train the model, you can generate them with the [code](https://github.com/wentaoyuan/pcn/tree/master/render) and [ShapeNetCore.v1](https://shapenet.org/). We generate 50 partial point clouds for each CAD model.

#### 4) Train or validate

Run `python3 val.py` to validate the model or `python3 train.py` to train the model from scratch.

### EMD

We provide an EMD implementation for point cloud comparison, which only needs $O(n)$ memory and thus enables dense point clouds  (with 10,000 points or over) and large batch size. It is based on an approximated algorithm (auction algorithm) and cannot guarantee a (but near) bijection assignment. It employs a parameter $\epsilon$ to balance the error rate and the speed of convergence. Smaller $\epsilon$ achieves more accurate results, but needs a longer time for convergence. The time complexity is $O(n^2k)$, where $k$ is the number of iterations. We set a $\epsilon = 0.005, k = 50$ during training and a $\epsilon = 0.002, k = 10000$ during testing. Please refer to`emd/README.md` for more details.

### Citation

If you find our work useful for your research, please cite:
```
@article{liu2019morphing,
  title={Morphing and Sampling Network for Dense Point Cloud Completion},
  author={Liu, Minghua and Sheng, Lu and Yang, Sheng and Shao, Jing and Hu, Shi-Min},
  journal={arXiv preprint arXiv:1912.00280},
  year={2019}
}
```

### License

This project Code is released under the Apache License 2.0 (refer to the LICENSE file for details).