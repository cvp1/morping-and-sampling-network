#!/usr/bin/env bash

IMAGE_NAME="morphing-and-sampling-network:latest"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # directory of this script

# Build base image
bash "$SCRIPT_DIR/docker_base_cuda/build-docker-base-image.sh"

# Build docker image
docker build -t "$IMAGE_NAME" "$SCRIPT_DIR"