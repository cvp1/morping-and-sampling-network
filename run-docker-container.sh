#!/usr/bin/env bash

IMAGE_NAME="morphing-and-sampling-network:latest"
CONTAINER_MOUNT_DIR="/app/ycb_data"
DEFAULT_HOST_MOUNT_DIR="$HOME/Desktop/CVP/pipeline"
HOST_MOUNT_DIR="${DEFAULT_HOST_MOUNT_DIR}"
MODEL_PATH="$CONTAINER_MOUNT_DIR/models/MSN_latest.pt"
PC_COMPLETE_PATH="$CONTAINER_MOUNT_DIR/pc_complete"
ENVIRONMENT="completion" # set this to "train", "test" or "completion" using the -e option

while getopts ":h:e:" opt; do
  case $opt in
    h) HOST_MOUNT_DIR="$OPTARG"
    ;;
    e) ENVIRONMENT="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

echo "MSN ENV variables:"
echo "  - ENVIRONMENT: ${ENVIRONMENT}"
echo "  - HOST_MOUNT_DIR: ${HOST_MOUNT_DIR}"
echo "  - INPUT_DIR: ${CONTAINER_MOUNT_DIR}"
echo "  - PC_COMPLETE_PATH: ${PC_COMPLETE_PATH}"
echo "  - MODEL_PATH: ${MODEL_PATH}"

# Run docker container
docker run \
 --user "$(id -u)":"$(id -g)" \
 -e "ENVIRONMENT=$ENVIRONMENT" \
 -e "INPUT_DIR=$CONTAINER_MOUNT_DIR" \
 -e "MODEL_PATH=$MODEL_PATH" \
 -e "PC_COMPLETE_PATH=$PC_COMPLETE_PATH" \
 --gpus all \
 -p 127.0.0.1:8097:8097/tcp \
 -v "$HOST_MOUNT_DIR":"$CONTAINER_MOUNT_DIR" \
 -it "$IMAGE_NAME"