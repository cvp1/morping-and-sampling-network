import argparse

import mlflow

from src.dataset import *  # please do not move dependency
from src.checkpoint_utils import create_checkpoint, load_checkpoint
from src.mlflow_utils import setup_mlflow_experiment, get_artifact_path, get_log_path
from src.model import *
from src.utils import *
import os
import visdom  # please do not move dependency
from time import time
import random
import torch.optim as optim

import src.emd.emd_module as emd

parser = argparse.ArgumentParser()
parser.add_argument('--batchSize', type=int, default=32, help='input batch size')
parser.add_argument('--partial_pc_path', type=str,
                    default='/home/steffi/dev/CVP/data/YCB_Video/pc_partial/preprocessed_full',
                    help='path to input PC')
parser.add_argument('--complete_pc_path', type=str,
                    default='/home/steffi/dev/CVP/data/YCB_Video/pc_complete',
                    help='path to complete PC')
parser.add_argument('--output_path', type=str,
                    default='/home/steffi/dev/CVP/data/YCB_Video/morphing_and_sampling_output',
                    help='path to output logs & network weights to')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=12)
parser.add_argument('--nepoch', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--model', type=str, default='', help='optional reload model path')
parser.add_argument('--num_points', type=int, default=2048, help='number of points')
parser.add_argument('--n_primitives', type=int, default=16, help='number of surface elements')
parser.add_argument('--env', type=str, default="MSN_TRAIN", help='visdom environment')

opt = parser.parse_args()


class FullModel(nn.Module):
    def __init__(self, model):
        super(FullModel, self).__init__()
        self.model = model
        self.EMD = emd.emdModule()

    def forward(self, inputs, gt, eps, iters):
        output1, expansion_penalty = self.model(inputs)
        gt = gt[:, :, :3]

        dist, _ = self.EMD(output1, gt, eps, iters)
        emd1 = torch.sqrt(dist).mean(1)

        return output1, emd1, expansion_penalty


vis = visdom.Visdom(port=8097, env=opt.env)  # set your port

# setup network
network = MorphingNet(num_points=opt.num_points, n_primitives=opt.n_primitives)
network = torch.nn.DataParallel(FullModel(network))
network.cuda()

# hyperparameters
best_val_loss = 10

# Loss average meters
train_loss = AverageValueMeter()
val_loss = AverageValueMeter()

# load existing weights if specified
if opt.model != '':
    model_state_dict, optimizer_state_dict, start_epoch, lrate, mlflow_run_id, experiment_name, random_seed = \
        load_checkpoint(opt.model)
    opt.manualSeed = random_seed
    network.module.model.load_state_dict(model_state_dict)
    optimizer = optim.Adam(network.module.model.parameters(), lr=lrate)
    optimizer.load_state_dict(optimizer_state_dict)
    print("Previous model and optimizer weights loaded.")
else:
    start_epoch = 0
    experiment_name = opt.env
    mlflow_run_id = None
    lrate = 0.001
    opt.manualSeed = random.randint(1, 10000)
    random.seed(opt.manualSeed)
    torch.manual_seed(opt.manualSeed)
    optimizer = optim.Adam(network.module.model.parameters(), lr=lrate)
    network.module.model.apply(weights_init)  # initialization of the weight
    print("New model and optimizer initialized.")

# create mlflow experiment
mlruns_path = os.path.join('file:', os.path.abspath(opt.output_path), 'mlruns')
if not os.path.exists(mlruns_path):
    os.makedirs(mlruns_path, exist_ok=True)
mlflow.set_tracking_uri(mlruns_path)
experiment = setup_mlflow_experiment(experiment_name)
run_name = str(int(time()))

# start mlflow run
with mlflow.start_run(run_id=mlflow_run_id, run_name=run_name, experiment_id=experiment.experiment_id):
    # mlflow run infos & paths
    active_run = mlflow.active_run()
    run_id = active_run.info.run_id
    artifact_path = get_artifact_path(active_run)

    # setup logging
    log_path = get_log_path(artifact_path)
    train_log_file = os.path.join(log_path, "train.log")
    network_log_file = os.path.join(log_path, "network.log")
    logger = setup_logging(train_log_file)
    if opt.model != '':
        logger.info(f"Resuming existing run {run_id} of experiment {experiment.experiment_id}.")
    else:
        logger.info(f"Starting new run {run_id} of experiment {experiment.experiment_id}.")
        # log passed training arguments from parser
        logging.info(f"Training arguments: {vars(opt)}")
        mlflow.log_params(vars(opt))

    # log network structure
    with open(network_log_file, 'a') as f:  # open and append
        f.write(str(network.module.model) + '\n')

    # load datasets
    dataloader = get_ycb_dataloader(data_file=os.path.join(get_project_root(), "src/data.json"),
                                    stage="train", partial_path=opt.partial_pc_path, complete_path=opt.complete_pc_path,
                                    batch_size=opt.batchSize, shuffle_data=True, num_workers=int(opt.workers))
    dataloader_test = get_ycb_dataloader(data_file=os.path.join(get_project_root(), "src/data.json"),
                                         stage="val", partial_path=opt.partial_pc_path,
                                         complete_path=opt.complete_pc_path,
                                         batch_size=opt.batchSize, shuffle_data=False, num_workers=int(opt.workers))
    len_dataset = len(dataloader.dataset)
    len_val_dataset = len(dataloader_test.dataset)
    logger.info(f"Train Set Size: {len_dataset}")
    logger.info(f"Validation Set Size: {len_val_dataset}")

    labels_generated_points = torch.Tensor(
        range(1, (opt.n_primitives + 1) * (opt.num_points // opt.n_primitives) + 1)).view(
        opt.num_points // opt.n_primitives, (opt.n_primitives + 1)).transpose(0, 1)
    labels_generated_points = (labels_generated_points) % (opt.n_primitives + 1)
    labels_generated_points = labels_generated_points.contiguous().view(-1)

    # Training loop
    for epoch in range(start_epoch, opt.nepoch):
        # TRAIN MODE
        train_loss.reset()
        network.module.model.train()

        # learning rate schedule
        if epoch == 20:
            optimizer = optim.Adam(network.module.model.parameters(), lr=lrate / 10.0)
        if epoch == 40:
            optimizer = optim.Adam(network.module.model.parameters(), lr=lrate / 100.0)

        for i, data in enumerate(dataloader, 0):
            optimizer.zero_grad()
            id, input, gt = data
            input = input.float().cuda()
            gt = gt.float().cuda()
            input = input.transpose(2, 1).contiguous()
            print(input.shape)

            output1, emd1, expansion_penalty = network(input, gt.contiguous(), 0.005, 50)
            loss_net = emd1.mean() + expansion_penalty.mean() * 0.1
            loss_net.backward()
            batch_emd_loss = emd1.mean().item()
            train_loss.update(batch_emd_loss)
            optimizer.step()

            if i % 5 == 0:
                idx = random.randint(0, input.size()[0] - 1)
                vis.scatter(X=gt.contiguous()[idx].data.cpu()[:, :3],
                            win='TRAIN_GT',
                            opts=dict(
                                title='TRAIN GT',
                                markersize=2,
                            ),
                            )
                vis.scatter(X=input.transpose(2, 1).contiguous()[idx].data.cpu(),
                            win='TRAIN_INPUT',
                            opts=dict(
                                title='TRAIN INPUT',
                                markersize=2,
                            ),
                            )
                vis.scatter(X=output1[idx].data.cpu(),
                            Y=labels_generated_points[0:output1.size(1)],
                            win='TRAIN_COARSE',
                            opts=dict(
                                title='TRAIN OUTPUT',
                                markersize=2,
                            ),
                            )

                mlflow.log_metrics({
                    "train_loss_batch": batch_emd_loss,
                    "train_loss_epoch": train_loss.avg,
                    "lrate": lrate
                })

                # ---- uncomment to test whether saving / loading of weights works  ----
                # create_checkpoint(network.module.model.state_dict(), optimizer, lrate,
                #                   logger, artifact_path, run_id, experiment_name, epoch, opt.manualSeed)

            logger.info(f'{opt.env} train [{epoch}: {i}/{int(len_dataset / opt.batchSize)}] \t'
                        f'emd: {batch_emd_loss:.8f} \t'
                        f'expansion_penalty: {expansion_penalty.mean().item():.8f}')

        # VALIDATION
        if epoch % 5 == 0:
            val_loss.reset()
            network.module.model.eval()
            with torch.no_grad():
                for i, data in enumerate(dataloader_test, 0):
                    id, input, gt = data
                    input = input.float().cuda()
                    gt = gt.float().cuda()
                    input = input.transpose(2, 1).contiguous()
                    output1, emd1, expansion_penalty = network(input, gt.contiguous(), 0.004, 3000)
                    batch_emd_loss = emd1.mean().item()
                    val_loss.update(batch_emd_loss)
                    idx = random.randint(0, input.size()[0] - 1)

                    if val_loss.val <= best_val_loss:
                        best_val_loss = val_loss.val

                    mlflow.log_metrics({
                        "val_loss_batch": batch_emd_loss,
                        "val_loss_epoch": val_loss.avg,
                        "best_val_loss": best_val_loss
                    })

                    vis.scatter(X=gt.contiguous()[idx].data.cpu()[:, :3],
                                win='VAL_GT',
                                opts=dict(
                                    title='VAL GT',
                                    markersize=2,
                                ),
                                )
                    vis.scatter(X=input.transpose(2, 1).contiguous()[idx].data.cpu(),
                                win='VAL_INPUT',
                                opts=dict(
                                    title='VAL INPUT',
                                    markersize=2,
                                ),
                                )
                    vis.scatter(X=output1[idx].data.cpu(),
                                Y=labels_generated_points[0:output1.size(1)],
                                win='VAL_COARSE',
                                opts=dict(
                                    title='VAL OUTPUT',
                                    markersize=2,
                                ),
                                )

                    logger.info(
                        f'{opt.env} val [{epoch}: {i}/{int(len_val_dataset / opt.batchSize)}] \t'
                        f'emd: {batch_emd_loss :.8f} \t'
                        f'expansion_penalty: {expansion_penalty.mean().item():.8f}'
                    )

        # save network and optimizer weights checkpoint
        create_checkpoint(network.module.model.state_dict(), optimizer,
                          lrate, logger, artifact_path, run_id, experiment_name, epoch, opt.manualSeed)
