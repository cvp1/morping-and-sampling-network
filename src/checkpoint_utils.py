import os
import torch


def load_checkpoint(checkpoint_path):
    checkpoint = torch.load(checkpoint_path, map_location=lambda storage, loc: storage)
    model_state_dict = checkpoint['model_state_dict']
    optimizer_state_dict = checkpoint['optimizer_state_dict']
    epoch = checkpoint['epoch']
    lrate = checkpoint['lrate']
    experiment_name = checkpoint['experiment_name']
    mlflow_run_id = checkpoint['mlflow_run_id']
    random_seed = checkpoint['random_seed']
    return model_state_dict, optimizer_state_dict, epoch, lrate, mlflow_run_id, experiment_name, random_seed


def create_checkpoint(state_dict,
                      optimizer,
                      lrate,
                      logger,
                      artifact_path,
                      mlflow_run_id,
                      experiment_name,
                      epoch,
                      random_seed):
    if epoch % 5 == 0:
        model_path = os.path.join(artifact_path, f"MSN_{str(epoch)}.pt")
    else:
        model_path = os.path.join(artifact_path, f"MSN_latest.pt")
    log_message = f"Saving model checkpoint to {model_path}."
    checkpoint = {
        'epoch': epoch,
        'model_state_dict': state_dict,
        'optimizer_state_dict': optimizer.state_dict(),
        'lrate': lrate,
        'experiment_name': experiment_name,
        'mlflow_run_id': mlflow_run_id,
        'random_seed': random_seed,
    }
    torch.save(checkpoint, model_path)
    logger.info(log_message)
