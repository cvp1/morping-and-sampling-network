#!/bin/bash

cd /app/src/emd || exit
python3 setup.py install
cd /app/src/expansion_penalty || exit
python3 setup.py install
cd /app/src/MDS || exit
python3 setup.py install
cd /app/src || exit