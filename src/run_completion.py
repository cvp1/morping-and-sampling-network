import os

from src.pc_watcher import PcWatcher

MODEL_PATH = 'MODEL_PATH'
INPUT_DIR = 'INPUT_DIR'
PC_COMPLETE_PATH = 'PC_COMPLETE_PATH'
ENV_VARS = [(MODEL_PATH, None), (INPUT_DIR, None), (PC_COMPLETE_PATH, None)]


def check_env_variables():
    for env_var, default_value in ENV_VARS:
        if os.getenv(env_var, default_value) is None:
            raise ValueError('Environment variable "{}" is unset'.format(env_var))


def get_input_dir():
    input_dir = os.path.abspath(os.getenv(INPUT_DIR))
    print('Point Cloud completion input_dir: {}'.format(input_dir))
    return input_dir


def main():
    check_env_variables()
    input_dir = get_input_dir()
    PcWatcher(input_dir).run()


if __name__ == "__main__":
    main()
