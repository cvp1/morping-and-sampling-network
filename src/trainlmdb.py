import random

if __name__ == '__main__':
    import argparse
    import torch
    import torch.optim as optim
    # from src.dataset import *
    from src.model import *
    from src.utils import *
    import os
    import json
    import datetime

    import src.emd.emd_module as emd

    from src.ycb_util.lmdb import lmdb_dataflow

    parser = argparse.ArgumentParser()
    parser.add_argument('--batchSize', type=int, default=50, help='input batch size')
    parser.add_argument('--train_lmdb', type=str, default="", help='path to train.lmdb')
    parser.add_argument('--val_lmdb', type=str, default="", help='path to val.lmdb')
    parser.add_argument('--workers', type=int, help='number of data loading workers', default=12)
    parser.add_argument('--nepoch', type=int, default=25, help='number of epochs to train for')
    parser.add_argument('--model', type=str, default='', help='optional reload model path')
    parser.add_argument('--num_points', type=int, default=2048, help='number of points')
    parser.add_argument('--n_primitives', type=int, default=16, help='number of surface elements')
    parser.add_argument('--env', type=str, default="MSN_TRAIN", help='visdom environment')

    opt = parser.parse_args()
    print(opt)


    class FullModel(nn.Module):
        def __init__(self, model):
            super(FullModel, self).__init__()
            self.model = model
            self.EMD = emd.emdModule()

        def forward(self, inputs, gt, eps, iters):
            output1, expansion_penalty = self.model(inputs)
            gt = gt[:, :, :3]

            dist, _ = self.EMD(output1, gt, eps, iters)
            emd1 = torch.sqrt(dist).mean(1)

            return output1, emd1, expansion_penalty


    now = datetime.datetime.now()
    save_path = datetime.datetime.timestamp(now)
    if not os.path.exists('./log/'):
        os.mkdir('./log/')
    dir_name = os.path.join('log', str(save_path))
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    logname = os.path.join(dir_name, 'log.txt')
    os.system('cp ./train.py %s' % dir_name)
    os.system('cp ./dataset.py %s' % dir_name)
    os.system('cp ./model.py %s' % dir_name)

    opt.manualSeed = random.randint(1, 10000)
    print("Random Seed: ", opt.manualSeed)
    random.seed(opt.manualSeed)
    torch.manual_seed(opt.manualSeed)
    best_val_loss = 10

    # dataset = YCBDataset(train=True)
    # dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize,
    #                                        shuffle=True, num_workers=int(opt.workers), drop_last=True)
    # dataset_test = YCBDataset(train=False)
    # dataloader_test = torch.utils.data.DataLoader(dataset_test, batch_size=opt.batchSize,
    #                                        shuffle=False, num_workers=int(opt.workers), drop_last=True)

    lmdb_train_path = opt.train_lmdb
    lmdb_val_path = opt.val_lmdb
    df_train, num_train = lmdb_dataflow.lmdb_dataflow(lmdb_train_path, opt.batchSize, opt.num_points, opt.num_points,
                                        is_training=True)
    train_generator = df_train.get_data()
    df_val, num_val = lmdb_dataflow.lmdb_dataflow(lmdb_val_path, opt.batchSize, opt.num_points, opt.num_points, is_training=False)
    val_generator = df_train.get_data()

    len_dataset = num_train
    print("Train Set Size: ", len_dataset)

    network = MorphingNet(num_points=opt.num_points, n_primitives=opt.n_primitives)
    network = torch.nn.DataParallel(FullModel(network))
    network.cuda()
    network.module.model.apply(weights_init)  # initialization of the weight

    if opt.model != '':
        network.module.model.load_state_dict(torch.load(opt.model))
        print("Previous weight loaded ")

    lrate = 0.001  # learning rate
    optimizer = optim.Adam(network.module.model.parameters(), lr=lrate)

    train_loss = AverageValueMeter()
    val_loss = AverageValueMeter()
    with open(logname, 'a') as f:  # open and append
        f.write(str(network.module.model) + '\n')

    train_curve = []
    val_curve = []
    labels_generated_points = torch.Tensor(
        range(1, (opt.n_primitives + 1) * (opt.num_points // opt.n_primitives) + 1)).view(
        opt.num_points // opt.n_primitives, (opt.n_primitives + 1)).transpose(0, 1)
    labels_generated_points = (labels_generated_points) % (opt.n_primitives + 1)
    labels_generated_points = labels_generated_points.contiguous().view(-1)

    for epoch in range(opt.nepoch):
        # TRAIN MODE
        train_loss.reset()
        network.module.model.train()

        # learning rate schedule
        if epoch == 20:
            optimizer = optim.Adam(network.module.model.parameters(), lr=lrate / 10.0)
        if epoch == 40:
            optimizer = optim.Adam(network.module.model.parameters(), lr=lrate / 100.0)

        for i, data in enumerate(train_generator, 0):
            point_cloud_ids, input, num_points, gt = data
            print(point_cloud_ids.shape)
            print(input.shape)
            print(num_points.shape)
            print(gt.shape)
            optimizer.zero_grad()
            input = torch.from_numpy(input)
            input = input.float().cuda()
            gt = torch.from_numpy(gt)
            gt = gt.float().cuda()
            input = input.transpose(2, 1).contiguous()

            output1, emd1, expansion_penalty = network(input, gt.contiguous(), 0.005, 50)
            loss_net = emd1.mean() + expansion_penalty.mean() * 0.1
            loss_net.backward()
            train_loss.update(emd1.mean().item())
            optimizer.step()

            print(opt.env + ' train [%d: %d/%d]  emd1: %f expansion_penalty: %f' % (
            epoch, i, len_dataset / opt.batchSize, emd1.mean().item(), expansion_penalty.mean().item()))
            train_curve.append(train_loss.avg)

        # VALIDATION
        if epoch % 5 == 0:
            val_loss.reset()
            network.module.model.eval()
            with torch.no_grad():
                for i, data in enumerate(val_generator, 0):
                    _, input, _, gt = data
                    input = input.float().cuda()
                    gt = gt.float().cuda()
                    input = input.transpose(2, 1).contiguous()
                    output1, emd1, expansion_penalty = network(input, gt.contiguous(), 0.004, 3000)
                    val_loss.update(emd1.mean().item())
                    print(opt.env + ' val [%d: %d/%d]  emd1: %f expansion_penalty: %f' % (
                    epoch, i, len_dataset / opt.batchSize, emd1.mean().item(), expansion_penalty.mean().item()))

        val_curve.append(val_loss.avg)

        log_table = {
            "train_loss": train_loss.avg,
            "val_loss": val_loss.avg,
            "epoch": epoch,
            "lr": lrate,
            "bestval": best_val_loss,
        }
        with open(logname, 'a') as f:
            f.write('json_stats: ' + json.dumps(log_table) + '\n')

        print('saving net...')
        torch.save(network.module.model.state_dict(), '%s/network.pth' % (dir_name))
