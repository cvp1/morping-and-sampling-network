import json
import os
import time

from watchdog.events import RegexMatchingEventHandler

from src.pc_completor import PointCloudCompletor
import torch.multiprocessing

KEY_CLASS_NAME = 'class_name'
CONFIG_SUFFIX = "-config.json"


class PcEventHandler(RegexMatchingEventHandler):
    PC_REGEX = [r".*pred_cloud\.pcd$"]

    def __init__(self):
        super().__init__(self.PC_REGEX)
        self.pc_completor = PointCloudCompletor()
        torch.multiprocessing.set_start_method('spawn')  # needed for multiprocessing context with CUDA

    def on_created(self, event):
        print("Processing {}".format(event.src_path))
        file_size = -1
        while file_size != os.path.getsize(event.src_path):
            file_size = os.path.getsize(event.src_path)
            time.sleep(1)
        self.trigger_point_cloud_completion_multiprocess(event.src_path)

    def get_ycb_object_class_name(self, pc_path):
        frame_config_name = self.extract_frame_id(pc_path) + CONFIG_SUFFIX
        frame_config_path = os.path.join(os.path.dirname(pc_path), frame_config_name)
        frame_config = self.get_frame_config(frame_config_path)
        return frame_config[KEY_CLASS_NAME]

    def extract_frame_id(self, pc_path):
        return os.path.basename(pc_path)[0:6]

    def get_frame_config(self, config_path):
        with open(config_path) as json_file:
            frame_config = json.load(json_file)
        return frame_config

    def trigger_point_cloud_completion(self, pc_path):
        ycb_object_class_name = self.get_ycb_object_class_name(pc_path)
        self.pc_completor.complete_point_cloud(pc_path, ycb_object_class_name)

    def trigger_point_cloud_completion_multiprocess(self, pc_path):
        process = torch.multiprocessing.Process(name='MSN_completion',
                                                target=self.trigger_point_cloud_completion,
                                                args=(pc_path,))
        process.start()
        print("My Process has terminated, terminating main thread")
        print("Terminating Child Process")
        process.join()
