from src.dataset import read_point_cloud, create_complete_pc_dict  # please keep this import at the top
from src.ycb_util.pc_util import write_pcd
from src.checkpoint_utils import load_checkpoint
from src.model import *
from src.utils import *
import argparse
import torch
import src.emd.emd_module as emd


parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, required=True, default='./tmp/network.pth',
                    help='Path to trained MSN model weights')
parser.add_argument('--complete_pc_path', type=str,
                    default='/app/ycb_data/pc_complete',
                    help='Path to ground truth PC directory')
parser.add_argument('--partial_pc_path', type=str, help='Path to PC to be completed')
parser.add_argument('--ycb_object_name', type=str, help='Class name of the YCB object to complete')

parser.add_argument('--num_points', type=int, default=2048, help='number of points')
parser.add_argument('--n_primitives', type=int, default=16, help='number of primitives in the atlas')


opt = parser.parse_args()
print(opt)

# Check use GPU or not
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
if device == "cpu":
    sys.exit("MSN cannot be used on CPU. Exiting.")

# network
network = MorphingNet(num_points=opt.num_points, n_primitives=opt.n_primitives)
network.to(device)
network.apply(weights_init)

# load checkpoint
model_state_dict, _, _, _, mlflow_run_id, experiment_name, _ = load_checkpoint(opt.model)
network.load_state_dict(model_state_dict)
print(f"Model weights loaded from experiment {experiment_name} and run {mlflow_run_id} for inference.")
network.eval()

EMD = emd.emdModule()

# create dictionary holding ground truth point cloud as numpy arrays, indexed by ycb object class name
ycb_object_classes = ["011_banana", "021_bleach_cleanser", "035_power_drill", "037_scissors"]
complete_pc_dict = create_complete_pc_dict(opt.complete_pc_path, ycb_object_classes)

output_dir = os.path.dirname(opt.partial_pc_path)
if not os.path.exists(output_dir):
    os.makedirs(output_dir, exist_ok=True)


def complete_point_cloud(point_cloud_file_path, ycb_object_class_name):
    print(f"Completing point cloud {point_cloud_file_path} for object class {ycb_object_class_name}")
    with torch.no_grad():
        gt = torch.from_numpy(complete_pc_dict[ycb_object_class_name]).float().to(device)
        gt = gt.unsqueeze(0).contiguous()

        partial = torch.from_numpy(read_point_cloud(point_cloud_file_path)).float().to(device)
        partial = partial.transpose(0, 1).unsqueeze(0).contiguous()

        output, expansion_penalty = network(partial)

        dist, _ = EMD(output, gt, 0.002, 10000)
        emd1 = torch.sqrt(dist).mean()
        emd_loss = emd1.mean().item()
        print(f"...... EMD loss for {point_cloud_file_path}: {emd_loss}")

        pcd_np = output[0].data.cpu().numpy()
        filename, _ = os.path.splitext(point_cloud_file_path)
        output_pc_file = os.path.join(output_dir, f"{os.path.basename(filename)}-completed.pcd")

        print(f"...... Saving completed point cloud to {output_pc_file}")
        write_pcd(pcd_np, output_pc_file)


if __name__ == "__main__":
    complete_point_cloud(opt.partial_pc_path, opt.ycb_object_name)
