import os
import sys

from src.utils import weights_init, timeit

from src.ycb_util.pc_util import write_pcd  # do not move dependency!
from src.dataset import create_complete_pc_dict, read_point_cloud

import torch  # torch imports needs to be after open3d import
from src.checkpoint_utils import load_checkpoint
import src.emd.emd_module as emd
from src.model import MorphingNet


MODEL_PATH = 'MODEL_PATH'
PC_COMPLETE_PATH = 'PC_COMPLETE_PATH'
INPUT_DIR = 'INPUT_DIR'
DEFAULT_YCB_CLASS_NAMES = ["011_banana", "021_bleach_cleanser", "035_power_drill", "037_scissors"]


class PointCloudCompletor:
    def __init__(self, num_points=2048, num_primitives=16, ycb_classes=None):
        self._model_path = os.getenv(MODEL_PATH)
        self._complete_pc_path = os.getenv(PC_COMPLETE_PATH)
        self._input_dir = os.getenv(INPUT_DIR)
        self._output_dir = self._input_dir

        self._ycb_classes = ycb_classes or DEFAULT_YCB_CLASS_NAMES
        self._complete_pcs = create_complete_pc_dict(self._complete_pc_path, self._ycb_classes)

        self._device = self.get_device()
        self.model = self._load_model(num_points, num_primitives)
        self.model.eval()
        self._emd = emd.emdModule()

    def get_device(self):
        # MSN doesn't work on CPU => check device
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if device == "cpu":
            sys.exit("MSN cannot be used on CPU. Exiting.")
        return device

    def _load_model(self, num_points, num_primitives):
        print("Initializing MSN model...")
        model = MorphingNet(num_points=num_points, n_primitives=num_primitives)
        model.to(self._device)
        model.apply(weights_init)

        # load checkpoint
        model_state_dict, _, _, _, mlflow_run_id, experiment_name, _ = load_checkpoint(self._model_path)
        model.load_state_dict(model_state_dict)
        print(f"Model weights loaded from experiment {experiment_name} and run {mlflow_run_id} for inference.")
        return model

    @timeit
    def complete_point_cloud(self, point_cloud_file_path, ycb_object_class_name):
        print(f"Completing point cloud {point_cloud_file_path} for object class {ycb_object_class_name}")
        with torch.no_grad():
            gt = torch.from_numpy(self._complete_pcs[ycb_object_class_name]).float().to(self._device)
            gt = gt.unsqueeze(0).contiguous()

            partial = torch.from_numpy(read_point_cloud(point_cloud_file_path)).float().to(self._device)
            partial = partial.transpose(0, 1).unsqueeze(0).contiguous()

            output, expansion_penalty = self.model(partial)

            dist, _ = self._emd(output, gt, 0.002, 10000)
            emd = torch.sqrt(dist).mean()
            emd_loss = emd.mean().item()
            print(f"...... EMD loss for {point_cloud_file_path}: {emd_loss}")

            pcd_np = output[0].data.cpu().numpy()
            filename, _ = os.path.splitext(point_cloud_file_path)
            output_pc_file = os.path.join(self._output_dir, f"{os.path.basename(filename)}-completed.pcd")

            print(f"...... Saving completed point cloud to {output_pc_file}")
            write_pcd(pcd_np, output_pc_file)
