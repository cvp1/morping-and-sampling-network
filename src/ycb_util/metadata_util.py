import fnmatch
import os

import numpy as np
import json

from scipy.io import loadmat

import sys
sys.path.insert(1, '../..')
from progress.bar import Bar

from config import get_project_root
from src.ycb_util.pc_util import _get_class_index


def get_unique_values(meta_json, key):
    return np.unique(extract_values_from_json(meta_json, key))


def extract_values_from_json(json, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(json, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(json, dict):
            for k, v in json.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(json, list):
            for item in json:
                extract(item, arr, key)
        return arr

    results = extract(json, arr, key)
    return results


def _get_meta_mat_path_list(data_path, is_synthetic=False):
    """
    Appends the first *-meta.mat file path in each video directory to a list and returns it.
    For data_syn, there are now video subdirectories and all *-meta.mat in the folder are processed.

    :param data_path: Path to data directory.
    :param is_synthetic: Whether the file path contains normal or synthetic YCB video frames.
    :return: List of *-meta.mat file paths (first in each video directory).
    """
    meta_mat_paths = list()
    
    if is_synthetic:
            for file in sorted(os.listdir(data_path)):
                if fnmatch.fnmatch(file, '*-meta.mat'):
                    meta_mat_paths.append(os.path.join(data_path, file))
    else:
        for video_dir, _, _ in os.walk(data_path):
            for file in sorted(os.listdir(video_dir)):
                if fnmatch.fnmatch(file, '*-meta.mat'):
                    meta_mat_paths.append(os.path.join(video_dir, file))
                    break
    return meta_mat_paths


def _get_num_frames(video_path, is_synthetic=False):
    """
    Returns the number of frames for a video path.
    :param video_path: Path to video frames.
    :param is_synthetic: Whether the file path contains normal or synthetic YCB video frames.
    :return: Number of frames in video path.
    """
    files_per_frame = 5
    if is_synthetic:
        files_per_frame = 4
    return int(len([file for file in os.listdir(video_path)]) / files_per_frame)


def _get_video_paths(object_class_ids=None, data_dir="", preprocessed_path="", is_synthetic=False):
    if object_class_ids is None:
        object_class_ids = [10, 12, 15, 17]  # banana, bleach, power drill, scissors
    data_path = os.path.abspath(data_dir)
    meta_mat_paths = _get_meta_mat_path_list(data_path, is_synthetic=is_synthetic)

    video_paths_by_id = {class_id: {} for class_id in object_class_ids}
    
    for object_class_id in object_class_ids:
        object_class_config = video_paths_by_id[object_class_id]
        if not is_synthetic:
            object_class_config["videos"] = dict()
        else:
            object_class_config["frame_idcs"] = list()
            object_class_config["cls_idcs"] = list()

        with Bar(f'Processing video meta data of class {object_class_id}:', max=len(meta_mat_paths)) as bar:
            for meta_mat_path in meta_mat_paths:
                meta_mat = loadmat(file_name=meta_mat_path, appendmat=False)
                video_path = os.path.dirname(meta_mat_path)
                video_dir_name = os.path.basename(video_path)
                class_index = _get_class_index(meta_mat, object_class_id)

                def _get_video_config():
                    video_config = {"num_frames": _get_num_frames(video_path, is_synthetic),
                                    "cls_index": int(class_index),
                                    "path": video_path,
                                    "preprocessed_path": os.path.join(preprocessed_path, video_path.split("/")[-1])}
                    return video_config

                if class_index is not None:
                    
                    if not is_synthetic:
                        object_class_config["videos"][video_dir_name] = _get_video_config()

                    else: # no subdict for syn data
                        if len(object_class_config["frame_idcs"]) == 0:
                            object_class_config.update(_get_video_config())
                        
                        syn_id = meta_mat_path.split("/")[-1].split("-")[0]
                        object_class_config["frame_idcs"].append(syn_id)
                        object_class_config["cls_idcs"].append(int(class_index))

                bar.next()
        
    return video_paths_by_id


def get_classes_config(classes_filepath,
                       object_class_ids,
                       data_dir="",
                       preprocessed_path="",
                       is_synthetic=False):
    """
    Returns a dictionary mapping from YCB object class id to class name.
    :param object_class_ids: Indeces of the object classes for which to create the config file.
    :param data_dir: Directory where YCB_video_dataset data is stored (path to /data dir).
    :param preprocessed_path: Directory to save the pre-processed point clouds for training to later on.
    :param classes_filepath: Path to the classes.txt file of the dataset ($YCB_PATH/image_sets/classes.txt)
    :return: Dictionary containing the mapping (int)class_id: (str)class_name
    :param is_synthetic: Whether the config should be created for normal or synthetic YCB video data.
    """
    classes_config = _get_video_paths(data_dir=data_dir,
                                      preprocessed_path=preprocessed_path,
                                      is_synthetic=is_synthetic)
    classes_strings = np.loadtxt(classes_filepath, dtype=str)
    # get only class names specified in object_class_ids
    object_class_names = [classes_strings[i - 1] for i in object_class_ids]

    for class_id, class_name in zip(object_class_ids, object_class_names):
        if class_id in classes_config:
            classes_config[class_name] = classes_config[class_id]
            classes_config[class_name]["id"] = str(class_id)
            del classes_config[class_id]
    return classes_config


def save_classes_config(classes_filepath,
                        object_class_ids=None,
                        data_dir="",
                        preprocessed_path="",
                        json_filepath='ycb_classes_config.json',
                        is_synthetic=False):
    """
    Save a JSON file containing the YCB classes config.
    :param object_class_ids: Indeces of the object classes for which to create the config file.
    :param data_dir: Directory where YCB_video_dataset data is stored (path to /data dir).
    :param preprocessed_path: Directory to save the pre-processed point clouds for training to later on.
    :param classes_filepath: Path to the classes.txt file of the dataset ($YCB_PATH/image_sets/classes.txt)
    :param json_filepath: Path to save the JSON output to.
    :param is_synthetic: Whether the config should be created for normal or synthetic YCB video data.

    The method saves a JSON file that contains an entry for each of the object classes specified.
    An entry for one class will look somewhat like this:

    "021_bleach_cleanser": { => object name from classes.txt
        "id": "12" => id / line number of the object from classes.txt
        "videos": [ => list of video objects
          {
            "num_frames": 248, => number of frames in this video
            "cls_index": 1, => index the object has in array containing "cls_indexes" (in *-meta.mat file)
            "path": "/Path/to/YCB video dataset/data/0018", => video path
            "preprocessed_path": "/path/to/save/preprocesses/data/to/0018"
          },
          {
            "num_frames": 4645,
            "cls_index": 1,
            "path": "/Path/to/YCB video dataset/data/0007",
            "preprocessed_path": "/path/to/save/preprocesses/data/to/0007"
          },
          {
            "num_frames": 4645,
            "cls_index": 3,
            "path": "/Path/to/YCB video dataset/data/0006",
            "preprocessed_path": "/path/to/save/preprocesses/data/to/0006"
          }
        ],
      }
    }

    If you are using synthetical data, there will be a list of frame_idcs and cls_idcs and no "videos" subdict. Like so:

    "037_scissors": {
        "frame_idcs": ["000003", "000008", "000017", "000018", "000019"],
        "cls_idcs": [0, 7, 4, 5, 5], 
        "num_frames": 80000, 
        "cls_index": 0, 
        "path": "/media/angelie/AK Elements/YCB_Video_Dataset/YCB_Video_Dataset/data_syn", 
        "preprocessed_path": "/home/angelie/Documents/University/Master_project/preprocessed_test/syn/data_syn", 
        "id": "17"}}

    """
    if object_class_ids is None:
        object_class_ids = [10, 12, 15, 17]  # banana, bleach, power drill, scissors
    classes_config = get_classes_config(classes_filepath, object_class_ids, data_dir,
                                        preprocessed_path, is_synthetic=is_synthetic)
    with open(json_filepath, 'w') as f:
        json.dump(classes_config, f)


if __name__ == '__main__':
    classes_file_path = "/media/angelie/AK Elements/YCB_Video_Dataset/YCB_Video_Dataset/image_sets/classes.txt"
    object_class_ids = [10, 12, 15, 17]
    data_dir = "/media/angelie/AK Elements/YCB_Video_Dataset/YCB_Video_Dataset/data_syn"
    preprocessed_dir = "/home/angelie/Documents/University/Master_project/preprocessed_test/syn"
    output_json_filepath = os.path.abspath(os.path.join(get_project_root(), 'ycb_classes_config.json'))
    save_classes_config(classes_filepath=classes_file_path,
                        object_class_ids=object_class_ids,
                        data_dir=data_dir,
                        preprocessed_path=preprocessed_dir,
                        json_filepath=output_json_filepath,
                        is_synthetic=True)
