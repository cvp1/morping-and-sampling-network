from itertools import filterfalse
from pathlib import Path
import json
import re

from sklearn.model_selection import train_test_split


def split_train_validation(paths, validation_size):
    return train_test_split(paths, test_size=validation_size, random_state=42)


def partition(pred, iterable):
    """Use a predicate to partition entries into false entries and true entries"""
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    return list(filterfalse(pred, iterable)), list(filter(pred, iterable))


def split_train_test(split):
    return partition(is_in_ycb_test_set, split)


def is_in_ycb_test_set(point_cloud_path):
    test_set_video_dirs = [f"{str(video_num).zfill(4)}/" for video_num in range(48, 60)]
    return any(test_set_video in point_cloud_path for test_set_video in test_set_video_dirs)


def get_relative_point_cloud_path(path):
    return f"{path.parts[-2]}/{path.name}"


def get_ycb_object_name(path: Path, classes: list):
    if not any(class_name in path.name for class_name in classes):
        return None
    return path.name.split("-")[1]


def get_ycb_object_rel_path(line):
    frame_id = re.findall("\d{4}\/\d{6}", line)[0]
    ycb_object_name = re.findall("(\d{3}\_\w*)", line)[0]
    return f"{frame_id}-{ycb_object_name}"


def get_paths_to_filter_out(log_file):
    with open(log_file, "r") as log_content:
        paths_to_filter_out = [get_ycb_object_rel_path(line) for line in log_content]
    return paths_to_filter_out


def is_in_paths_to_filter_out(path: Path, paths_to_filter_out):
    return any(path_to_filter_out in str(path) for path_to_filter_out in paths_to_filter_out)


def should_be_added(ycb_object_name, path, paths_to_filter_out):
    should_path_be_filtered = paths_to_filter_out is not None and is_in_paths_to_filter_out(path, paths_to_filter_out)
    return ycb_object_name is not None and not should_path_be_filtered


def save_ycb_data_split_counts(data_file: str, output_file: str):
    """
    Save a JSON file containing the number of point clouds for each YCB object in the train / validation / test splits.
    :param data_file: File holding the train / validation / test split.
    :param output_file: Output file to save counts to.
    """
    counts = {}
    with open(data_file) as json_data:
        data = json.load(json_data)
        for split_name in data.keys():
            split_count = 0
            counts[split_name] = {}
            split = data[split_name]
            for ycb_class_label in split.keys():
                ycb_class_pc_count = len(split[ycb_class_label])
                counts[split_name][ycb_class_label] = ycb_class_pc_count
                split_count = split_count + ycb_class_pc_count
                print(f"Class data count for object {ycb_class_label} in split {split_name} is {ycb_class_pc_count}")
            counts[split_name]["total"] = split_count
            print(f"Total data count for split {split_name} is {split_count}")
    with open(output_file, "w") as counts_data:
        json.dump(counts, counts_data)


def save_ycb_data_splits(data_root_dir: str,
                         ycb_class_names: str,
                         output_path: str,
                         validation_size: float,
                         log_file_path=None,
                         pc_ext=".pcd"):
    """
    Split the YCB point cloud data for the given classes into train / val / test partitions.
    The test partition is created according to the original YCB Video dataset specs (video numbers 0048-0059).
    The training and validation data are split taking the validation_size into account.

    :param data_root_dir:      root directory of processed ycb PC folders
    :param ycb_class_names:    Class names of the YCB objects to use as comma-separated string,
                               e.g. '011_banana,021_bleach_cleanser'
    :param output_path:        Output path (including name) of the JSON file to store the splits in.
    :param validation_size:    Fraction of the data (not including test data) to use for validation.
    :param log_file_path:      Path to the log file used for filtering data. If None => no filtering.
    :param pc_ext:             Extension of the point cloud files in the data_root_dir. Default: '*.pcd'
    """
    ycb_classes = {ycb_class_name: [] for ycb_class_name in ycb_class_names.split(",")}

    splits = {
        "train": {},
        "val": {},
        "test": {}
    }

    paths_to_filter_out = None
    if log_file_path is not None:
        paths_to_filter_out = get_paths_to_filter_out(log_file_path)

    for path in Path(data_root_dir).rglob(f"*{pc_ext}"):
        ycb_object_name = get_ycb_object_name(path, list(ycb_classes.keys()))
        if should_be_added(ycb_object_name, path, paths_to_filter_out):
            ycb_classes[ycb_object_name].append(get_relative_point_cloud_path(path))

    for ycb_class_label, paths in ycb_classes.items():
        paths_train, paths_test = split_train_test(paths)
        paths_train, paths_validation = split_train_validation(paths_train, validation_size)
        splits["train"][ycb_class_label] = paths_train
        splits["test"][ycb_class_label] = paths_test
        splits["val"][ycb_class_label] = paths_validation

    with open(output_path, "w") as datafile:
        json.dump(splits, datafile)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("data_root_dir", type=str, help="Root directory of the data")
    parser.add_argument("--ycb_classes", type=str, default="011_banana,021_bleach_cleanser,035_power_drill,037_scissors",
                        help="Class names of the YCB objects to use as comma-separated string,"
                             " e.g. '011_banana,021_bleach_cleanser'")
    parser.add_argument("--output_path_data", type=str, default="./data.json",
                        help="Output path (including name) of the JSON file "
                             "to store the train / validation / test split in.")
    parser.add_argument("--output_path_counts", type=str, default="./data_split_counts.json",
                        help="Output path (including name) of the JSON file "
                             "to store the train / validation / test split in.")
    parser.add_argument("--filter_with_log", type=bool, default=True)
    parser.add_argument("--log_path", type=str, default="../preprocessing_pipeline/data_full_log/train_pc_preprocessing.log")
    parser.add_argument("--validation_size", type=float, default=0.1, help="Size of validation split. Default is 10%.")

    args = parser.parse_args()
    log_file = None
    if args.filter_with_log:
        log_file = args.log_path
    save_ycb_data_splits(args.data_root_dir, args.ycb_classes, args.output_path_data, args.validation_size, log_file)
    save_ycb_data_split_counts(args.output_path_data, args.output_path_counts)
