import json
import os

import torch
import torch.utils.data as data
import numpy as np

from src.ycb_util.pc_util import read_point_cloud


class YCBDataset(data.Dataset):
    def __init__(self, data_file: str, partial_path: str, complete_path: str, ycb_object_classes: str, stage="train"):
        assert stage in ["train", "val", "test"]
        self.partial_path = partial_path
        self.complete_path = complete_path
        self.ycb_object_classes = ycb_object_classes.split(",")

        with open(data_file, 'r') as datafile:
            splits = json.load(datafile)
            self.model_list = np.concatenate(list(splits[stage].values()))

        np.random.shuffle(self.model_list)
        self.len = self.model_list.size
        self.index = 0
        self.complete_pc_dict = self._create_complete_pc_dict(complete_path, self.ycb_object_classes)

    @staticmethod
    def _create_complete_pc_dict(path, ycb_object_classes):
        return {ycb_object: read_point_cloud(os.path.join(path, f"{ycb_object}.pcd"))
                for ycb_object in ycb_object_classes}

    @staticmethod
    def _get_ycb_object_name(model_list, index):
        return model_list[index].split('-')[1]

    def __getitem__(self, index):
        ycb_object_name = self._get_ycb_object_name(self.model_list, index)
        assert ycb_object_name in self.complete_pc_dict

        complete = torch.from_numpy(self.complete_pc_dict[ycb_object_name]).float()
        partial = read_point_cloud(os.path.join(self.partial_path, self.model_list[index]))

        return partial, complete

    def __len__(self):
        return self.len


def get_ycb_dataloader(data_file="./data.json",
                       partial_path='/home/steffi/dev/CVP/data/YCB_Video/pc_partial/preprocessed_full',
                       complete_path='/home/steffi/dev/CVP/ycb-video-preprocessing/ycb_video_groundtruths',
                       ycb_object_classes="011_banana,021_bleach_cleanser,035_power_drill,037_scissors",
                       stage="train",
                       batch_size=32,
                       shuffle_data=True,
                       num_workers=12):
    """Get PyTorch dataloader for processed YCB data.

    :param data_file: str: path to JSON data file
    :param partial_path: str: path to directory containing folders of YCB point clouds
    :param complete_path: str: path to directory containing class folders with complete point clouds
    :param ycb_object_classes: string containing the object classes contained in the data set.
    :param stage: str: training, validation or test stage => train, val, test
    :param batch_size: int: number of point_clouds per batch
    :param shuffle_data: boolean: if 'True' shuffle data each epoch
    :param num_workers: int: number of subprocesses loading data
    :return torch.utils.data.DataLoader
    """
    dataset = YCBDataset(data_file, partial_path, complete_path, ycb_object_classes, stage="train")
    return torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                       shuffle=shuffle_data, num_workers=num_workers, drop_last=True)


if __name__ == "__main__":
    get_ycb_dataloader()
