#  Customized from: https://github.com/wentaoyuan/pcn/blob/master/data_util.py
import argparse

import numpy as np
from tensorpack import dataflow


class BatchData(dataflow.ProxyDataFlow):
    def __init__(self, ds, batch_size, input_size, gt_size, remainder=False, use_list=False):
        super(BatchData, self).__init__(ds)
        self.batch_size = batch_size
        self.input_size = input_size
        self.gt_size = gt_size
        self.remainder = remainder
        self.use_list = use_list

    def __len__(self):
        ds_size = len(self.ds)
        div = ds_size // self.batch_size
        rem = ds_size % self.batch_size
        if rem == 0:
            return div
        return div + int(self.remainder)

    def __iter__(self):
        holder = []
        for data in self.ds:
            holder.append(data)
            if len(holder) == self.batch_size:
                yield self._aggregate_batch(holder, self.use_list)
                del holder[:]
        if self.remainder and len(holder) > 0:
            yield self._aggregate_batch(holder, self.use_list)

    def _aggregate_batch(self, data_holder, use_list=False):
        """
        Concatenate input points along the 0-th dimension
        Stack all other data along the 0-th dimension
        """
        point_cloud_ids = np.stack([pc_id for pc_id, _, _ in data_holder])
        num_points = np.full(point_cloud_ids.size, self.input_size).astype(np.int32)
        partial_point_clouds = \
            np.expand_dims(np.concatenate([partial_pc for _, partial_pc, _ in data_holder]), 0).astype(np.float32)
        ground_truth_point_clouds = np.stack([gt_pc for _, _, gt_pc in data_holder]).astype(np.float32)
        return point_cloud_ids, partial_point_clouds, num_points, ground_truth_point_clouds


def lmdb_dataflow(lmdb_path, batch_size, input_size, output_size, is_training, test_speed=False):
    df = dataflow.LMDBSerializer.load(lmdb_path, shuffle=False)
    size = df.size()
    if is_training:
        df = dataflow.LocallyShuffleData(df, buffer_size=2000)
        df = dataflow.PrefetchData(df, num_prefetch=500, num_proc=1)
    df = BatchData(df, batch_size, input_size, output_size)
    if is_training:
        df = dataflow.PrefetchData(df, 50, num_proc=8)
    df = dataflow.RepeatedData(df, -1)
    if test_speed:
        dataflow.TestDataSpeed(df, size=1000).start()
    df.reset_state()
    return df, size


if __name__ == '__main__':
    # Speed test of dataflow
    # lmdb_path = "/home/steffi/dev/CVP/ycb-video-preprocessing/src/ycb_util/lmdb/train.lmdb"
    # batch_size = 64
    # input_size = output_size = 2048
    # is_training = False
    # test_speed = True
    # lmdb_dataflow(lmdb_path, batch_size, input_size, output_size, is_training, test_speed)


    parser = argparse.ArgumentParser()
    parser.add_argument('--lmdb_path', type=str,
                        default="/home/steffi/dev/CVP/ycb-video-preprocessing/src/ycb_util/lmdb/train.lmdb",
                        help='path to .lmdb file')
    args = parser.parse_args()

    # Create generator for training data
    lmdb_path = args.lmdb_path
    batch_size = 32
    input_size = output_size = 2048
    df_train, num_train = lmdb_dataflow(lmdb_path, batch_size, input_size, output_size, is_training=True)
    train_generator = df_train.get_data()
    # in your training loop, use the generator as follows
    for epoch in range(1):
        point_cloud_ids, partial_point_clouds, num_points, gt_point_cloud = next(train_generator)
        print(f"epoch: {epoch}")
        first_id = point_cloud_ids[30]
        print(f"first_id: {first_id}")
        print(f"point_cloud_ids: {point_cloud_ids}")
        print(f"point_cloud_ids.shape: {point_cloud_ids.shape}")
        # partial_point_clouds holds a concatenated array
        # of all partial pc in this batch (length = num_points_partial * batch_size)
        print(f"partial_point_clouds.shape: {partial_point_clouds.shape}")
        print(f"num_points.shape: {num_points.shape}")
        print(f"gt_point_cloud.shape: {gt_point_cloud.shape}")
        print()

