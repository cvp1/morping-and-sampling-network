import argparse
import json
import os
import numpy as np
from pathlib import Path

from tensorpack import DataFlow, dataflow

from config import get_project_root
from src.ycb_util.pc_util import read_point_cloud


class YCBPointCloudDataFlow(DataFlow):
    def __init__(self,
                 data_json_file,
                 ycb_object_classes: str,
                 partial_dir,
                 complete_dir,
                 stage="train",
                 complete_pc_ext=".pcd"):

        self.partial_dir = partial_dir
        self.complete_dir = complete_dir
        self.complete_pc_ext = complete_pc_ext
        self.ycb_object_classes = ycb_object_classes.split(",")
        self.complete_pc_dict = self._get_complete_pc_dict()
        with open(data_json_file, 'r') as datafile:
            splits = json.load(datafile)
            self.model_list = np.concatenate(list(splits[stage].values()))
        np.random.shuffle(self.model_list)

    def size(self):
        return self.model_list.size

    def _get_complete_pc_dict(self):
        complete_pc_dict = {}
        for pc_path in Path(self.complete_dir).rglob(f"*{self.complete_pc_ext}"):
            ycb_object_name = pc_path.name.split(".")[0]
            assert ycb_object_name in self.ycb_object_classes, f"YCB object name of ground truth point cloud {pc_path}"\
                                                               f" not present in self.ycb_object_classes."
            complete_pc_dict[ycb_object_name] = read_point_cloud(str(pc_path))
        return complete_pc_dict

    def get_data(self):
        for model_id in self.model_list:
            ycb_object_name = model_id.split("-")[1]
            if ycb_object_name not in self.complete_pc_dict:
                continue
            complete_pc = self.complete_pc_dict[ycb_object_name]
            partial_pc = read_point_cloud(os.path.join(self.partial_dir, model_id))
            yield model_id.replace('/', '_'), partial_pc, complete_pc


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_json_path', default=os.path.join(get_project_root(), "src/ycb_util/data.json"),
                        help="Path to your data.json file")
    parser.add_argument('--stage', default="train", help="Stage: train, val or test")
    parser.add_argument("--ycb_classes", type=str,
                        default="011_banana,021_bleach_cleanser,035_power_drill,037_scissors",
                        help="Class names of the YCB objects to use as comma-separated string,"
                             " e.g. '011_banana,021_bleach_cleanser'")
    parser.add_argument('--partial_dir', help="Directory where the partial point clouds are stored.")
    parser.add_argument('--complete_dir', help="Directory where the complete point clouds are stored.")
    parser.add_argument('--output_path',
                        help="Path to the file in which to store the output (example: /path/to/train.lmdb)")
    args = parser.parse_args()

    output_path = args.output_path or os.path.abspath(f"./{args.stage}.lmdb")

    data_flow = YCBPointCloudDataFlow(args.data_json_path, args.ycb_classes,
                                      args.partial_dir, args.complete_dir, args.stage)
    if os.path.exists(output_path):
        os.system('rm %s' % args.output_path)
    dataflow.LMDBSerializer.save(data_flow, output_path)
