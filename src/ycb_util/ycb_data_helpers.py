import os
import json
import random
import open3d as o3d
import numpy as np
import torch
import torch.utils.data as data

class YCBDataset(data.Dataset): 
    def __init__(self, data_file, partial_path, complete_path, train = True):
        self.train = train
        self.partial_path = partial_path
        self.complete_path = complete_path

        with open(data_file, 'r') as datafile:
            splits = json.load(datafile)
            if self.train:
                self.model_list = [path for paths in splits['train'].values() for path in paths]
            else:
                self.model_list = [path for paths in splits['val'].values() for path in paths]

        random.shuffle(self.model_list)
        self.model_list = self.model_list
        self.len = len(self.model_list)
        self.index = 0

    def __getitem__(self, index):
        def read_pcd(filename):
            pcd = o3d.io.read_point_cloud(filename)
            return torch.from_numpy(np.array(pcd.points)).float()
        
        partial = read_pcd(os.path.join(self.partial_path, self.model_list[index]))
        complete = read_pcd(os.path.join(self.complete_path, self.model_list[index].split('-')[-2], 'points_downsampled.pcd'))

        return partial, complete

    def __len__(self):
        return self.len


def get_ycb_dataloader(data_file="./data/data.json", partial_path='./data/prepocessed/preprocessed_full', 
                       complete_path='./data/models', train=True, batch_size=32, shuffle_data=True, num_workers=12):
    """Get PyTorch dataloader for processed YCB data.

    :param data_file: str: path to JSON data file
    :param partial_path: str: path to directory containing folders of YCB point clouds
    :param complete_path: str: path to directory containing class folders with complete point clouds
    :param train: boolean: if 'True' take train set, else validation set
    :param batch_size: int: number of point_clouds per batch
    :param shuffle_data: boolean: if 'True' shuffle data each epoch
    :param num_workers: int: number of subprocesses loading data
    :return torch.utils.data.DataLoader
    """
    dataset = YCBDataset(data_file, train, partial_path, complete_path)
    return torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                       shuffle=shuffle_data, num_workers=num_workers, drop_last=True)