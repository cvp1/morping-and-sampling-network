import os
import time

from watchdog.observers import Observer

from src.pc_event_handler import PcEventHandler


MODEL_PATH = 'MODEL_PATH'


def check_env_variables():
    if os.getenv(MODEL_PATH) is None:
        raise ValueError('Environment variable "MODEL_PATH" is unset')


class PcWatcher:
    def __init__(self, src_path):
        self.__src_path = src_path
        self.__event_handler = PcEventHandler()
        self.__event_observer = Observer()

    def run(self):
        print("Watching directory {}".format(self.__src_path))
        self.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.stop()

    def start(self):
        self.__schedule()
        self.__event_observer.start()

    def stop(self):
        self.__event_observer.stop()
        self.__event_observer.join()

    def __schedule(self):
        self.__event_observer.schedule(
            self.__event_handler,
            self.__src_path,
            recursive=True
        )

