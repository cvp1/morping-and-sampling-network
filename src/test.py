from src.dataset import get_ycb_dataloader  # please keep this import at the top
from src.checkpoint_utils import load_checkpoint
from src.model import *
from src.utils import *
import argparse
import torch
import visdom
import src.emd.emd_module as emd

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, required=True, default='./trained_model/network.pth', help='optional reload model path')
parser.add_argument('--partial_pc_path', type=str,
                    default='/app/ycb_data/pc_partial/preprocessed_full',
                    help='Path to input PC')
parser.add_argument('--complete_pc_path', type=str,
                    default='/app/ycb_data/pc_complete',
                    help='Path to complete PC')
parser.add_argument('--output_path', type=str,
                    default='/app/ycb_data/msn_test',
                    help='Path to output directory for test.py (for log files etc.)')
parser.add_argument('--batch_size', type=int, default=8, help='Batch size to use for the test DataLoader.')
parser.add_argument('--num_points', type=int, default=2048, help='number of points')
parser.add_argument('--n_primitives', type=int, default=16, help='number of primitives in the atlas')
parser.add_argument('--env', type=str, default="MSN_TEST", help='visdom environment')

# Check use GPU or not
use_gpu = torch.cuda.is_available()  # use GPU
if use_gpu:
    device = torch.device("cuda")
else:
    device = torch.device("cpu")
print(f"Using device: {device}")

opt = parser.parse_args()
print(opt)
vis = visdom.Visdom(port=8097, env=opt.env)

network = MorphingNet(num_points=opt.num_points, n_primitives=opt.n_primitives)
network.cuda()
network.apply(weights_init)

# load checkpoint
model_state_dict, _, _, _, mlflow_run_id, experiment_name, _ = load_checkpoint(opt.model)
network.load_state_dict(model_state_dict)
print(f"Previous model weights loaded from experiment {experiment_name} and run {mlflow_run_id}.")
network.eval()

# create output directory
output_path = os.path.abspath(opt.output_path)
if not os.path.exists(output_path):
    os.makedirs(output_path, exist_ok=True)

# setup logging
test_log_file = os.path.join(output_path, f"test_{experiment_name}_{mlflow_run_id}.log")
if os.path.isfile(test_log_file):
    print(f"Log file {test_log_file} exists already. Exiting.")
    sys.exit(1)
logger = setup_logging(test_log_file)

# load test data
dataloader_test = get_ycb_dataloader(data_file=os.path.join(get_project_root(), "src/data.json"),
                                     stage="test",
                                     partial_path=opt.partial_pc_path,
                                     complete_path=opt.complete_pc_path,
                                     batch_size=opt.batch_size,
                                     shuffle_data=False)
len_test_dataset = len(dataloader_test.dataset)
logger.info(f"Test Set Size:{len_test_dataset}")

vis = visdom.Visdom(port=8097, env=opt.env)  # set your port

EMD = emd.emdModule()

labels_generated_points = torch.Tensor(
    range(1, (opt.n_primitives + 1) * (opt.num_points // opt.n_primitives) + 1)).view(
    opt.num_points // opt.n_primitives, (opt.n_primitives + 1)).transpose(0, 1)
labels_generated_points = (labels_generated_points) % (opt.n_primitives + 1)
labels_generated_points = labels_generated_points.contiguous().view(-1)

# Average loss meter
test_loss = AverageValueMeter()

with torch.no_grad():
    for i, data in enumerate(dataloader_test, 0):
        object_id, partial, gt = data
        partial = partial.float().cuda()
        gt = gt.float().cuda()
        partial = partial.transpose(2, 1).contiguous()

        output1, expansion_penalty = network(partial)
        dist, _ = EMD(output1, gt, 0.002, 10000)
        emd1 = torch.sqrt(dist).mean()
        batch_emd_loss = emd1.mean().item()
        test_loss.update(batch_emd_loss)
        object_frame = str(object_id[0])
        vis.scatter(X=gt[0].data.cpu(), win='GT',
                    opts=dict(title=f"GT_{object_frame}", markersize=2))
        vis.scatter(X=partial.transpose(2, 1).contiguous()[0].data.cpu(), win='INPUT',
                    opts=dict(title=f"INPUT_{object_frame}", markersize=2))
        vis.scatter(X=output1[0].data.cpu(),
                    Y=labels_generated_points[0:output1.size(1)],
                    win='COARSE',
                    opts=dict(title=f"OUTPUT_{object_frame}", markersize=2))
        logger.info(f'{opt.env} test [{i+1}/{int(len_test_dataset / opt.batch_size)}] \t'
                    f'emd: {batch_emd_loss:.8f} \t'
                    f'expansion_penalty: {expansion_penalty.mean().item():.8f}')
    logging.info(f"Mean test emd loss: {test_loss.avg}")
