import numpy as np
import cupy as cp

"""
Scripts for farthest point sampling from Point Clouds.
Source of original: https://codereview.stackexchange.com/questions/179561/farthest-point-algorithm-in-python
"""

def _calc_distances(p0, points):
    return ((p0 - points) ** 2).sum(axis=1)


def farthest_points_downsampling(points, num_points=2048):
    """
    Farthest point downsampling function for NumPy arrays.
    @Angelie => Look into the function below if you have a GPU available 🚀
    :param points: Numpy array of Point Cloud points.
    :param num_points: Number of points the final downsampled PC should have.
    :return: farthest_points: NumPy array containing the downsampled PC.
    """
    farthest_points = np.zeros((num_points, 3))
    farthest_points[0] = points[np.random.randint(len(points))]
    distances = _calc_distances(farthest_points[0], points)
    for i in range(1, num_points):
        farthest_points[i] = points[np.argmax(distances)]
        distances = np.minimum(distances, _calc_distances(farthest_points[i], points))
    return farthest_points


def farthest_point_downsampling_cupy(points_cp, num_points=2048):
    """
    CuPy version of the above. This code can be executed on GPU and save you loads of time! 🚀🌈
    For installation instructions see: https://docs-cupy.chainer.org/en/stable/install.html#install-cupy
    :param points_cp: CuPy array of PC points. Can be created using cp.array(np_array|list)
    :param num_points: Number of points the final downsampled PC should have.
    :return: farthest_points: CuPy array containing the downsampled PC.
    """
    farthest_points = cp.zeros((num_points, 3))
    farthest_points[0] = points_cp[cp.random.randint(len(points_cp))]
    distances = _calc_distances(farthest_points[0], points_cp)
    for i in range(1, num_points):
        farthest_points[i] = points_cp[cp.argmax(distances)]
        distances = cp.minimum(distances, _calc_distances(farthest_points[i], points_cp))
    return farthest_points