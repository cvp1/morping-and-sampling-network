#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # directory of this script
ENVIRONMENT_TRAIN="train"
ENVIRONMENT_TEST="test"
ENVIRONMENT_COMPLETION="completion"

# activate venv
. /app/.venv/bin/activate

# run visdom server & wait 3 seconds for it to start
visdom &
sleep 5

# Check what the environment variable $ENVIRONMENT is set to and execute script accordingly
if [ "$ENVIRONMENT" == "$ENVIRONMENT_TRAIN" ]; then
    echo "Starting training..."
    bash "$SCRIPT_DIR/run_training.sh"
elif  [[ "$ENVIRONMENT" == "$ENVIRONMENT_TEST" ]]; then
    echo "Starting test..."
    bash "$SCRIPT_DIR/run_test.sh"
elif  [[ "$ENVIRONMENT" == "$ENVIRONMENT_COMPLETION" ]]; then
    echo "Starting completion run..."
    bash "$SCRIPT_DIR/run_completion.sh"
else
    echo "Unknown environment string passed. Exiting."
    exit 1
fi