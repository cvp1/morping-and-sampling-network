#!/usr/bin/env bash

# run training script
python -m src.train.py \
  --partial_pc_path /app/ycb_data/pc_partial/preprocessed_full \
  --complete_pc_path /app/ycb_data/pc_complete \
  --output_path /app/ycb_data/morphing_and_sampling_output \
  --num_points 2048 \
  --batchSize 32