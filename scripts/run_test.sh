#!/usr/bin/env bash

# run test script
python -m src.test.py \
  --partial_pc_path /app/ycb_data/pc_partial/preprocessed_full \
  --complete_pc_path /app/ycb_data/pc_complete \
  --model /app/ycb_data/models/MSN_latest.pt
